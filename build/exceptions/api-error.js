"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("../definitions/common");
class ApiError extends Error {
    constructor(status = common_1.httpStatus.internalServerError, message = '', errors = {}) {
        super();
        this.name = '';
        this.message = '';
        this.errors = {};
        this.status = common_1.httpStatus.internalServerError;
        this.status = status;
        if (common_1.apiErrorCodes.has(status)) {
            this.name = common_1.apiErrorCodes.get(this.status);
        }
        if (Object.keys(errors).length) {
            this.errors = errors;
        }
        this.message = message;
    }
}
exports.default = ApiError;
//# sourceMappingURL=api-error.js.map